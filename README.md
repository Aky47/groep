GROEP - Infographic, De Boor's Boat
----------------
The projects in this documentation were created using [Node](http://nodejs.org), [Javascript](https://developer.mozilla.org/fr/docs/Web/JavaScript), [WebGl](https://webglfundamentals.org/).


Organisation
-------------
Please see the video presentation (see index on the right) for an overview of the video project.

In the same index you will find the functions and classes used in the project.


Usage
------
To test the project for yourself, just click **[here](./../index.html)**.


Authors
------
GÉRARD Charles, DUBOIS Théo, VANDEMOORTELE Alexis, WARTEL Thibault.


Bonne correction !
Team GROEP

![Alt Gif du bateau](boat.gif)

