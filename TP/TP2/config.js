/**
 * @file config.js is the file that configure the WebGl canva, and update it when user click on the canva.
 * @version 1.0
 * @author Groep Team
 * @module TP2
 * @copyright 2021
 */

var allControlPoints = new Array; 
var isConstructEnabled = true;

/**
 * Create shader 
 * @param {Object} gl - 
 * @param {Object} type - 
 * @param {Object} source - 
 * @returns {shader} 
 */
function createShader(gl, type, source) {
  var shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (success) {
    return shader;
  }

  console.log(gl.getShaderInfoLog(shader));
  gl.deleteShader(shader);
}


/**
 * Create Program 
 * @param {Object} gl - 
 * @param {Object} vertexShader - 
 * @param {Object} fragmentShader - 
 * @returns {program} 
 */
function createProgram(gl, vertexShader, fragmentShader) {
  var program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  var success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (success) {
    return program;
  }

  console.log(gl.getProgramInfoLog(program));
  gl.deleteProgram(program);
}



/**
 *  main   
 *  this function is the heart of the webGL program loaded on this page
 *  allows to determine which Shadersource should webGL uses
 *  is call upon page loading
 */
function main() {
  // Get A WebGL context
  var canvas = document.getElementById('glCanvas');
  var gl = canvas.getContext("webgl");
  if (!gl) {
    return;
  }

  // Get the strings for our GLSL shaders
  var vertexShaderSource = document.querySelector("#vertex-shader-2d").text;
  var fragmentShaderSource = document.querySelector("#fragment-shader-2d").text;

  // create GLSL shaders, upload the GLSL source, compile the shaders
  var vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource);
  var fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource);

  // Link the two shaders into a program
  var program = createProgram(gl, vertexShader, fragmentShader);

  // look up where the vertex data needs to go.
  var positionAttributeLocation = gl.getAttribLocation(program, "a_position");
  var fColorLocation = gl.getUniformLocation(program,"fColor");

  // Create a buffer and put three 2d clip space points in it
  var positionBuffer = gl.createBuffer();

  // Bind it to ARRAY_BUFFER (think of it as ARRAY_BUFFER = positionBuffer)
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  var nbrPoints = 3;
  var positions = circle_getPoints(nbrPoints);
  //var positions = epicycloid_getPoints(nbrPoints,8,5);

  //var positions = [[0, 0], [0,0.5 ], [0.7, 0]]
  
  var positions = [
    0, 0,
    0, 0.5,
    0.7, 0,
  ];

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

  // code above this line is initialization code.
  // code below this line is rendering code.

  webglUtils.resizeCanvasToDisplaySize(gl.canvas);

  // Tell WebGL how to convert from clip space to pixels
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

  // Clear the canvas
  gl.clearColor(1, 1, 1, 1);
  gl.clear(gl.COLOR_BUFFER_BIT);

  // Tell it to use our program (pair of shaders)
  gl.useProgram(program);

  // Turn on the attribute
  gl.enableVertexAttribArray(positionAttributeLocation);

  // Bind the position buffer.
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
  var size = 2;          // 2 components per iteration
  var type = gl.FLOAT;   // the data is 32bit floats
  var normalize = false; // don't normalize the data
  var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
  var offset = 0;        // start at the beginning of the buffer
  gl.uniform4f(fColorLocation,0,1,0,1)
  gl.vertexAttribPointer(
      positionAttributeLocation, size, type, normalize, stride, offset);

  // draw
  var primitiveType = gl.TRIANGLE_FAN;
  var offset = 0;
  var count = nbrPoints;
  gl.drawArrays(primitiveType, offset, count);

  return fColorLocation
}

var fColorLocation = main();



/**
 *  resetPoints
 *  function only called when the user wants to reset every points he added so far
 *  is called when the button associated is pressed 
 */
function resetPoints(){

  // reset points of control
  allControlPoints = []
  ctx.clearRect(0, 0, cw, ch);
  circles = [];
  lastId = 0;

}


/**
 *  clearCanvas
 *  function called when the user wants to clear the webGl Canvas
 *  also called by some others function
 */
function clearCanvas(){
  // Clear the canvas

  var canvas = document.getElementById("glCanvas");
  var gl = canvas.getContext("webgl");
  if (gl != null){
   gl.clearColor(1, 1, 1, 1);
   gl.clear(gl.COLOR_BUFFER_BIT);

  }
  else{
    console.log("Clearing failed...")
  }

}


/**
 *  draw
 * This function draws a line between an array of points on the webGL canvas
 *
 *
 * 
 * @param {int} nbrPoints number of points given, more points mean a nicer shape
 * @param {Array} positions Array of points, a line will be drawn between them
 * @returns {null} nothing
 */
function draw(nbrPoints,positions){

  var canvas = document.querySelector("#glCanvas");
  var gl = canvas.getContext("webgl");
  
  var color = getColorValueFromHTML();

  gl.uniform4f(fColorLocation,color[0],color[1],color[2],1)
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);
  var primitiveType = gl.LINE_STRIP;
  var offset = 0;
  var count = nbrPoints;
  gl.drawArrays(primitiveType, offset, count);
  


}


/**
 *  getColorValueFromHTML
 * this function returns the value of the HTML color picker to determine
 * the color of the curve to draw
 *
 *
 * 
 *
 * @returns {Array} Array of 3 values ranging from 0 to 1 (r,g,b)
 */
function getColorValueFromHTML(){
  var color = document.getElementById("curveColor").value
  var hex_code = color.split("");
  var red = parseInt(hex_code[1]+hex_code[2],16);
  var green = parseInt(hex_code[3]+hex_code[4],16);
  var blue = parseInt(hex_code[5]+hex_code[6],16);
  return ([red/255,green/255,blue/255])
}


/**
 *  toggleConstruction
 *  This function influcences the global var "isConstructEnabled"
 *  allowing the program to also draw the construction steps aferwards
 *
 *
 */
function toggleConstruction(){
  isConstructEnabled = !isConstructEnabled;
  document.getElementById("toggleValue").innerHTML = isConstructEnabled
  _drawConstruction()
}




/**
 *  drawConstructionPrimitive
 * This function will draw every line and points of construction 
 *
 *
 * 
 * @param {gl} gl WebGL's canvas used to draw
 * @param {Array} arr Control points
 * @param {undefined} primitiveType Type of draw that WebGL will execute (likely gl.POINTS or gl.LINE_STRIP)
 * @param {Array} color Color in which the points will be 
 * @returns {null} nothing
 */
function drawConstructPrimitive(gl,arr,primitiveType,color){


  gl.uniform4f(fColorLocation,color[0]/255,color[1]/255,color[2]/255,1)
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(arr), gl.STATIC_DRAW);
  var offset = 0;
  gl.drawArrays(primitiveType, offset, arr.length/2);

}


/**
 *  drawConstruction
 * This function will call sub-functions to draw on the HTML canvas
 * every line and points necesscecary to the construction of the bezierCurve
 *
 *
 * 
 * @param {int} n number of points given, more points mean a nicer shape
 * @param {Array} pts Control points
 * @param {int} t specific value of t (to get a given points)
 * @returns {Array} Array of array storing each points of construction (1st array = depth 0 , 2nd array = depth 1 etc...)  
 *                    this includes the control points given in parameter !
 */
function drawConstruction(n,pts,t){

  var canvas = document.querySelector("#glCanvas");
  var gl = canvas.getContext("webgl");

  let cons = getConstructionPoints(t,pts); //Construction points
  let iter = 0


  //for each constructions array
  cons.forEach(elem =>
    {
      iter++;

      currentColor = getNextRGB(iter,cons.length)

      drawConstructPrimitive(gl,elem,gl.LINE_STRIP,currentColor) //Lines drawn
      drawConstructPrimitive(gl,elem,gl.POINTS,currentColor) //points drawn
    });

}








/**
 * Determine current RGB color for construction points display, according to saturation of colors.
 * @param {int} degree - current degree of the points
 * @param {int} degreeMax - maximum degree evaluated
 * @returns {Array} Array of R/G/B code
 */
function getNextRGB(degree, degreeMax){

  let satLevel = Math.trunc(20/degreeMax) * degree;
  //console.log("degree : ",degree,"\n degree max : ",degreeMax)

  let r = 255 - (10 * (20-satLevel));
  let g = 6 * (20-satLevel);
  let b = 6 * (20-satLevel);

  
  return [r,g,b];
}







/**
 * Determine click position, according to ratio between HTML and canva.
 * Push results (x,y) in array "allControlPoints"
 * @param {int} x - x component
 * @param {int} y - y component
 * @returns {Array} Array of float containing the new x and y 
 */
function switchCoordinateSyst(x,y){

  // (1) Center the data on the origin: 
  x -= cw/2;
  y -= ch/2;

  // (2) Scale it down by the same amount in both dimensions, such that the larger of the two ranges becomes 
  x /= cw/2;
  y /= ch/2;

  y*=-1; //because it is inverted

    return([x,y])

}

/**
 * Determine click position, according to ratio between HTML and canva.
 * Push results (x,y) in array "allControlPoints"
 * @param {Object} canvas - webgl canvas
 * @param {Object} event - mouse clic event
 */
function getMousePosition(canvas, event) { 
    let rect = canvas.getBoundingClientRect(); 
    let x = event.clientX - rect.left; 
    let y = event.clientY - rect.top; 
   // console.log("Coordinate x: " + x,  "Coordinate y: " + y); 

    let sw = switchCoordinateSyst(x,y)

    x = sw[0];
    y = sw[1];


    allControlPoints.push(x);
    allControlPoints.push(y);

    onHTMLCanvasChange()
    console.log("All Points : "+ allControlPoints);
} 

/**
 * Wrapper function for the drawConstruction function
 * Serve to initialise parameters properly
 * @param {Object} canvas - webgl canvas
 * @param {Object} event - mouse clic event
 */
function _drawConstruction(){
  let nbr = document.getElementById("n").value;
  let t = document.getElementById("tValue").value;
  clearCanvas();
  draw(nbr,casteljau(nbr,allControlPoints));
  //If we want the construction points to appear
  if(isConstructEnabled){
    drawConstruction(nbr,allControlPoints,t)
  }
}

function onHTMLCanvasChange(){
  _drawConstruction()
}

document.getElementById("tValue").onchange = function() {
  _drawConstruction()
}

document.getElementById("slider").onchange = function() {
  _drawConstruction()
}

document.getElementById("curveColor").onchange = function() {
  draw()
}

var canvasElem = document.getElementById("glCanvas"); 

//when the user clicks on the canvas:
//It triggers an auto drawing function
canvasElem.addEventListener("mousedown", function(e) 
{ 
  getMousePosition(canvasElem, e); 
}); 


document.getElementById('draw').addEventListener("click", function() 
{ 
  let nbr = document.getElementById("n").value;
  p = casteljau(nbr,allControlPoints); 
  
  draw(nbr,p)
  _drawConstruction()

}); 

document.getElementById('clear').addEventListener('click', clearCanvas);
document.getElementById('reset').addEventListener('click', resetPoints);
document.getElementById('toggleConstruction').addEventListener('click', toggleConstruction);



document.getElementById("slider").oninput = function() {
  document.getElementById("tValue").value = this.value/1000;
} 




