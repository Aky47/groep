/**
 * @file utils.js contains functions that gives the points of a circle and an epycycloid.
 * @version 1.0
 * @author Groep Team
 * @module TP2
 * @copyright 2021
 */

/**
 *  circle_getPoints
 *  Returns an array of n points describing a circle
 * 
 * @param {int} n number of points
 * @returns {Array} An array containing all X,Y values. The X are associated to the pair index, and the Y, associated to the impair index.
 *                     
 */
function circle_getPoints(n){

    let positions = new Array();
    let x;
    let y;
    //positions.push(0,0);
    //Loop appending a new point every iteration
    for(i=0;i<n;i++){

        x = 3 * Math.cos( (i*2*Math.PI ) / n);  // we normalize by n 
        y = 3 * Math.sin( (i*2*Math.PI ) / n); 

        x = x / 3; // we divide by 3 to get a x between -1 and 1 (which is the range allowed to render a point in WebGL)
        y = y / 3; // we divide by 3 to get a y between -1 and 1
        
        positions.push(x,y);
    }
    return positions;
}


/**
 *  circle_getPoints
 *  Returns an array of n points describing a epicycloid
 * 
 *
 * 
 * @param {int} a Parameter used in the formula
 * @param {int} b Parameter used in the formula
 * @param {int} width Width of the canvas, to normalize the figure
 * @param {int} height Height of the canvas, to normalize the figure
 * @returns {Array} Unidimensional array storing successively each x and y coordinate of every point
 *                     
 */
function epicycloid_getPoints(a,b,width,height){

    let n = 1000;
    let positions = new Array();
    let x;
    let y;
    //positions.push(0,0);
    //Loop appending a new point every iteration
    for(i=0;i<n;i++){

        x = (a + b) * Math.sin(i/10) - b * Math.sin((a/b + 1) * i/10);  // we normalize by n 
        y = (a + b) * Math.cos(i/10) - b * Math.cos((a/b + 1) * i/10); 
        
        x/=width;
        y/=height;
        x*=9;
        y*=9;
        
        positions.push(x,y);
    }
    console.log(positions);
    return positions;
}