/**
 * 
 * @file casteljau.js is the JS file that contains casteljau algorithm
 * @see https://fr.wikipedia.org/wiki/Algorithme_de_Casteljau
 * @version 1.0
 * @author Groep Team
 * @module TP2
 * @copyright 2021
 */



/**
 *  Returns an array of n points describing a parametric curve
 *  using De Casteljau's algorithm - 
 *  The idea is to use this algorithm for a n amount of points.
 *
 * 
 * @param {int} n Number of points used to draw the curve, more points means a well rounded curve
 * @param {Array} pts Control points
 * @returns {Array} Unidimensional array storing successively each x and y coordinate of every point
*/
function casteljau(n,pts){

    //Final array to return
    let bezierPoints = []

    let buffer = [...pts];//store pts in buffer

    //This loop is used
    for(j=0; j<n; j++){
        let t = j/n //so that t is between [0;1]
        let buffer = [...pts]
        a = [...pts];



      //while we've not reached the end of casteljau's algorithm
       while(a.length > 2){
        
            // the variable "buffer" represents the next step of "a" in casteljau's algorithm and therefor 
            // is used to construct the new points stored in "a"

            a = [...buffer];

            buffer = []
            for (let i = 0; i < a.length - 2; i+=2){

                buffer[i]   = a[i] * (1-t) + a[i+2] * t; //x component of the new point calculated 
                buffer[i+1] = a[i+1] * (1-t) + a[i+3] * t; //y component of the new point calculated
            }



        }

        //Once this function arrives here, "a" contains 1 point (with a x component and a y component)
        //hence the following lines :
        bezierPoints.push(a[0]);
        bezierPoints.push(a[1]);
    
    }
    return bezierPoints

}


/**
 *  getConstructionPoints
 * This function returns every steps that casteljau's algorithm does
 * for a specific t moment given
 * Most likely used to draw step on the canvas 
 *
 *
 * 
 * @param {int} t specific value of t (to get a given points)
 * @param {Array} pts Control points
 * @returns {Array} Array of array storing each points of construction (1st array = depth 0 , 2nd array = depth 1 etc...)  
 *                    this includes the control points given in parameter !
 */
function getConstructionPoints(t,pts){

     //variable to return
     let constructionPoints = [];


      buffer = [...pts];
      a = [...pts];

    //while we've not reached the end of casteljau's algorithm
    while(a.length > 2){
    
        // the variable "buffer" represents the next step of "a" in casteljau's algorithm and therefor 
        // is used to construct the new points stored in "a"

        constructionPoints.push(buffer);

        a = [...buffer];

        buffer = []
        for (let i = 0; i < a.length - 2; i+=2){

            buffer[i]   = a[i] * (1-t) + a[i+2] * t; //x component of the new point calculated 
            buffer[i+1] = a[i+1] * (1-t) + a[i+3] * t; //y component of the new point calculated
        }



    }
    return constructionPoints;
}
