/**
 * @file overlay.js is a file that manage canva drawing
 * @version 1.0
 * @author Groep Team
 * @module TP2
 * @copyright 2021
 */


// canvas related variables
// references to canvas and its context and its position on the page
var canvas = document.getElementById("overlayCanvas");
var ctx = canvas.getContext("2d");
var offsetX = canvas.getBoundingClientRect().left;
var offsetY = canvas.getBoundingClientRect().top;
let cw = canvas.getBoundingClientRect().width;
let ch = canvas.getBoundingClientRect().height;
canvas.width=cw;
canvas.height=ch;
ctx.font = '15px  serif';

// flag to indicate a drag is in process
// and the last XY position that has already been processed
var isDown = false;
var lastX;
var lastY;
var lastId = 0;

// the radian value of a full circle is used often, cache it
var PI2 = Math.PI * 2;

// variables relating to existing circles
var circles = [];
var stdRadius = 11;
var draggingCircle = -1;

// clear the canvas and redraw all existing circles
function drawAll() {
    ctx.clearRect(0, 0, cw, ch);
    var textId = String;
    for (var i = 0; i < circles.length; i++) {
        textId = i;
        var circle = circles[i];
        ctx.beginPath();
        ctx.arc(circle.x, circle.y, circle.radius, 0, PI2);
        ctx.closePath();
        ctx.fillStyle = 'rgb(200,200,200)';
        ctx.fill();
        ctx.fillStyle = "Black";
        ctx.fillText(textId, circle.x-4, circle.y+4, 20);
        
      
    }
}

function handleMouseDown(e) {
    // tell the browser we'll handle this event
    e.preventDefault();
    e.stopPropagation();

    // save the mouse position
    // in case this becomes a drag operation
    lastX = parseInt(e.clientX - offsetX);
    lastY = parseInt(e.clientY - offsetY);

    // hit test all existing circles
    var hit = -1;
    for (var i = 0; i < circles.length; i++) {
        var circle = circles[i];
        var dx = lastX - circle.x;
        var dy = lastY - circle.y;
        if (dx * dx + dy * dy < circle.radius * circle.radius) {
            hit = i;
        }
    }

    // if no hits then add a circle
    // if hit then set the isDown flag to start a drag
    if (hit < 0) {

        //Propagate the mouse event onto the webGL canvas
        getMousePosition(canvasElem,e)

        circles.push({
            x: lastX,
            y: lastY,
            radius: stdRadius,
            id: lastId,
        });
        lastId++;
        drawAll();
    } else {
        draggingCircle = circles[hit];
        isDown = true;
    }

}

function handleMouseUp(e) {
    // tell the browser we'll handle this event
    e.preventDefault();
    e.stopPropagation();

    // stop the drag
    isDown = false;
}

function handleMouseMove(e) {

    // if we're not dragging, just exit
    if (!isDown) {
        return;
    }

    // tell the browser we'll handle this event
    e.preventDefault();
    e.stopPropagation();

    // get the current mouse position
    mouseX = parseInt(e.clientX - offsetX);
    mouseY = parseInt(e.clientY - offsetY);

    // calculate how far the mouse has moved
    // since the last mousemove event was processed
    var dx = mouseX - lastX;
    var dy = mouseY - lastY;

    // reset the lastX/Y to the current mouse position
    lastX = mouseX;
    lastY = mouseY;

    // change the target circles position by the 
    // distance the mouse has moved since the last
    // mousemove event
    draggingCircle.x += dx;
    draggingCircle.y += dy;

    //Change the control points
    allControlPoints[draggingCircle.id * 2] = switchCoordinateSyst(draggingCircle.x,0)[0]
    allControlPoints[draggingCircle.id * 2 +1] = switchCoordinateSyst(0,draggingCircle.y)[1]

    //Change the webGl Canvas
    onHTMLCanvasChange()
    // redraw all the circles
    drawAll();
}

// listen for mouse events
canvas.addEventListener("mousedown", function (e) {
    handleMouseDown(e);
});
  
canvas.addEventListener("mousemove", function (e) {
    handleMouseMove(e);
});
  
canvas.addEventListener("mouseup", function (e) {
    handleMouseUp(e);
});
canvas.addEventListener("mouseout", function (e) {
    handleMouseUp(e);
});