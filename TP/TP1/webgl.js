/**
 * @file webgl.js in TP1 is the file that manage the webgl canva.
 * @version 1.0
 * @author Groep Team
 * @module TP1
 * @copyright 2021
 */


/* eslint no-console:0 consistent-return:0 */
"use strict";

/**
 * Créer un shader et il en faut des 2 types (gl est le contexte, type est soit vertex soit fragment, soirce est...)
 * @param {*} gl 
 * @param {*} type 
 * @param {*} source 
 */
function createShader(gl, type, source) {
  var shader = gl.createShader(type);
  gl.shaderSource(shader, source);
  gl.compileShader(shader);
  var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
  if (success) {
    return shader;
  }

  console.log(gl.getShaderInfoLog(shader));
  gl.deleteShader(shader);
}

/**
 * 
 * @param {*} gl 
 * @param {*} vertexShader 
 * @param {*} fragmentShader 
 */
function createProgram(gl, vertexShader, fragmentShader) {
  var program = gl.createProgram();
  gl.attachShader(program, vertexShader);
  gl.attachShader(program, fragmentShader);
  gl.linkProgram(program);
  var success = gl.getProgramParameter(program, gl.LINK_STATUS);
  if (success) {
    return program;
  }

  console.log(gl.getProgramInfoLog(program));
  gl.deleteProgram(program);
}


/**
 * Fonction qui permet de setup le buffer avant de l'afficher
 * @param {*} x 
 * @param {*} y 
 * @param {*} nPoints 
 * @param {*} valueRange 
 * @param {*} positionBuffer 
 * @param {*} gl 
 */
function setupDrawParametricFunction(x, y, nPoints, valueRange, positionBuffer, gl) {
  let xMax = 0, yMax = 0;
  let xMin = 0, yMin = 0;
  let xTmp = 0, yTmp = 0;

  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
  let positions = [];

  // On ajoute chacun des points dans le tableau
  for (let i = 0; i < nPoints; i++) {
    xTmp = x((valueRange * i) / nPoints); // Entre 0 et valueRange
    yTmp = y((valueRange * i) / nPoints);

    // Identification max/min x
    if (xTmp > xMax) xMax = xTmp;
    else if (xTmp < xMin) xMin = xTmp;

    // Identification max/min y
    if (yTmp > yMax) yMax = yTmp;
    else if (yTmp < yMin) yMin = yTmp;

    positions.push(xTmp);
    positions.push(yTmp);
    console.log(positions.length)
  }

  // On calcul le décalage de notre centre par rapport à 0 en x et y
  const xOffset = xMin + (xMax - xMin) / 2;
  const yOffset = yMin + (yMax - yMin) / 2;

  // Le ration par lequel on va diviser nos valeurs afin que cela soit dans l'intervalle [-1, 1]
  // Si l'écart entre les y est plus grand que celui entre les x on redimensionnera relativement au y, sinon au x
  const normalizationRatio = (yMax - yMin > xMax - xMin) ?
    (
      (yMax - yMin) / 2
    ) : (
      (xMax - xMin) / 2
    );

  for (let i = 0; i < 2 * nPoints; i++) {
    // Si le point est une coordonée en x (i % 2 == 0), on lui applique le décalage en x
    // Sinon on lui applique le décallage en y
    // Puis on effectue la mise à l'échelle (normalisation)
    positions[i] = (i % 2 == 0) ? 
      (
        (positions[i] - xOffset) / normalizationRatio
      ) : (
        (positions[i] - yOffset) / normalizationRatio
      );
  }
  return positions;
}

function main() {
  const canvas = document.querySelector("#glCanvas");
  const gl = canvas.getContext("webgl");

  if (!gl) {
    alert("ERROR WEBGL INIT FROM MAINJS");
    return;
  }

  // Redimensionne notre canvas pour qu'il ai une taille adapté à ce que l'on veut afficher
  var vertexShaderSource = document.querySelector("#vertex-shader-2d").text;
  var fragmentShaderSource = document.querySelector("#fragment-shader-2d").text;
  var vertexShader = createShader(gl, gl.VERTEX_SHADER, vertexShaderSource); // Afficher points / figures
  var fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, fragmentShaderSource); // Couleurs, ombre, etc.
  // Crée le programme à partir des vertexs
  var program = createProgram(gl, vertexShader, fragmentShader);
  // On défini l'attribut que l'on utlise pour la position
  var positionAttributeLocation = gl.getAttribLocation(program, "a_position");
  // Comme attribut nécessition un buffer, on le crée
  var positionBuffer = gl.createBuffer();

  // On dot à webgl d'utiliser program pour l'exécuter plus tard
  gl.useProgram(program);
  // On active l'attribut de position
  gl.enableVertexAttribArray(positionAttributeLocation);

  // Bind the position buffer
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  const figure = document.getElementById("figure").value;
  const n = document.getElementById("n").value;
  let positions;

  if (figure == "circle") {
    const valueRange = 2*Math.PI;
    const x = (t) => 3*Math.cos(t);
    const y = (t) => 3*Math.sin(t);
    positions = setupDrawParametricFunction(x, y, n, valueRange, positionBuffer, gl);
  }
  else if (figure == "epicycloid") {
    const a = parseInt(document.getElementById("a").value);
    const b = parseInt(document.getElementById("b").value);
    const valueRange = 100;
    const x = (t) => (a + b) * Math.sin(t) - b * Math.sin((a / b + 1) * t) + 20;
    const y = (t) => (a + b) * Math.cos(t) - b * Math.cos((a / b + 1) * t) + 20;
    positions = setupDrawParametricFunction(x, y, n, valueRange, positionBuffer, gl);
  }

  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);


  webglUtils.resizeCanvasToDisplaySize(gl.canvas);
  
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

  // Définir la couleur d'effacement comme étant le noir, complétement opaque
  gl.clearColor(0.125, 0.133, 0.145, 1.0);
  // Effacer le tampon de couleur avec la couleur d'effacement spécifiée
  gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);

  // Bind the position buffer.
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

  // Tell the attribute how to get data out of positionBuffer (ARRAY_BUFFER)
  const size = 2;
  const type = gl.FLOAT;
  const normalize = false;
  const stride = 0;
  const offset = 0;
  gl.vertexAttribPointer(positionAttributeLocation, size, type, normalize, stride, offset);

  // Draw
  const primitiveType = (figure == 'circle') ? gl.LINE_LOOP : gl.LINE_STRIP; // Ne relie pas le dernier point avec le premier
  gl.drawArrays(primitiveType, offset, n);
}


/**
 * 
 */
function clearCanvas(){
  // Clear the canvas

  console.log("Clearing...")
  let canvas = document.getElementById("glCanvas");
  let gl = canvas.getContext("webgl");
  if (gl != null){
    gl.clearColor(0.125, 0.133, 0.145, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    console.log("Cleared!")
  }
  else console.log("Clearing failed...")
}



document.getElementById('clear').addEventListener('click', clearCanvas);
document.getElementById('draw').addEventListener('click', main);
