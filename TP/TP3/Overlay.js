/**
 * @file Overlay.js
 * @version Overlay.js 1.0
 * @module TP3 - De Boor
 */


var allControlPoints = new Array; 

/**
 * Determine click position, according to ratio between HTML and canva.
 * Push results (x,y) in array "allControlPoints"
 * @param {Object} canvas - webgl canvas
 * @param {Object} event - mouse clic event
 */
 function getMousePosition(_canvas, event) { 
  let rect = _canvas.getBoundingClientRect(); 
  let x = event.clientX - rect.left; 
  let y = event.clientY - rect.top; 
  let sw = switchCoordinateSyst(x,y)

  x = sw[0];
  y = sw[1];

  allControlPoints.push(x);
  allControlPoints.push(y);

  changeValues();
} 


/**
 * Determine click position, according to ratio between HTML and canva.
 * Push results (x,y) in array "allControlPoints"
 * @param {Number} x - x component
 * @param {Number} y - y component
 * @returns {Array<Number>} Array of float containing the new x and y 
 */
 function switchCoordinateSyst(x,y) {
  // (1) Center the data on the origin: 
  x -= cw/2;
  y -= ch/2;
  // (2) Scale it down by the same amount in both dimensions, such that the larger of the two ranges becomes 
  x /= cw/2;
  y /= ch/2;

  y*=-1; //because it is inverted

  return([x,y])
}

// When the user clicks on the canvas:
// It triggers an auto drawing function
let canvasElem = document.getElementById("glCanvas"); 
canvasElem.addEventListener("mousedown", function(e) { 
  getMousePosition(canvasElem, e); 
}); 


// Canvas related variables
// References to canvas and its context and its position on the page
var canvas = document.getElementById("overlayCanvas");
var ctx = canvas.getContext("2d");
var offsetX = canvas.getBoundingClientRect().left;
var offsetY = canvas.getBoundingClientRect().top;
let cw = canvas.getBoundingClientRect().width;
let ch = canvas.getBoundingClientRect().height;
canvas.width=cw;
canvas.height=ch;
// flag to indicate a drag is in process
// and the last XY position that has already been processed
var isDown = false; //click?
var lastX;  //Mousexy
var lastY;
var lastId = 0; //last point added
// the radian value of a full circle is used often, cache it
var PI2 = Math.PI * 2;
// variables relating to existing circles
var circles = []; // all circle point 1 cirlce = (x,y,r,id)
var stdRadius = 10; 
var draggingCircle = -1;


/**
 * Clear the canvas and redraw all existing circles
 */
function drawAll() {
  let textId = String;
  ctx.clearRect(0, 0, cw, ch); // Erase before drawing
  for (let i = 0; i < circles.length; i++) {
    textId = i;
    let circle = circles[i];
    ctx.beginPath(); // start draw
    ctx.arc(circle.x, circle.y, circle.radius, 0, PI2); // draw complete circle from 0 to pi^2
    ctx.closePath();
    ctx.fillStyle = 'rgb(200,200,200)';
    ctx.fill();
    ctx.fillStyle = "Black";
    ctx.fillText(textId, circle.x-4, circle.y+4, 20);
  }
}

/**
 * Handle the event, then draw a new point, or draw the drag point at it new position.
 * Finally, undrag the point.
 * @param {MouseEvent} e 
 */
function handleMouseDown(e) {
  // tell the browser we'll handle this event
  e.preventDefault();
  e.stopPropagation();

  // save the mouse position
  // in case this becomes a drag operation
  lastX = parseInt(e.clientX - offsetX);
  lastY = parseInt(e.clientY - offsetY);

  // hit test all existing circles (if != -1)
  let hit = -1;
  for (let i = 0; i < circles.length; i++) {
    let circle = circles[i];
    let dx = lastX - circle.x;
    let dy = lastY - circle.y;
    if (dx * dx + dy * dy < circle.radius * circle.radius) {
        hit = i;
    }
  }

  // if no hits then add a circle
  // if hit then set the isDown flag to start a drag
  if (hit < 0) {
    //Propagate the mouse event onto the webGL canvas
    getMousePosition(canvasElem, e)
    circles.push({
        x: lastX,
        y: lastY,
        radius: stdRadius,
        id: lastId,
    });
    lastId++;
    drawAll();
  } else {
    draggingCircle = circles[hit];
    isDown = true;
  }
}


/**
 * Handle the event, then stop the drag
 * @param {MouseEvent} e 
 */
function handleMouseUp(e) {
  // tell the browser we'll handle this event
  e.preventDefault();
  e.stopPropagation();
  // stop the drag
  isDown = false;
}


/**
 * Handle the event, then draw a new point, or draw the drag point at it new position
 * @param {MouseEvent} e
 * @returns {null} - If we're not dragging
 */
function handleMouseMove(e) {

    // if we're not dragging, just exit
    if (!isDown) return;

    // tell the browser we'll handle this event
    e.preventDefault();
    e.stopPropagation();

    // get the current mouse position
    let mouseX = parseInt(e.clientX - offsetX);
    let mouseY = parseInt(e.clientY - offsetY);

    // calculate how far the mouse has moved
    // since the last mousemove event was processed
    var dx = mouseX - lastX;
    var dy = mouseY - lastY;

    // reset the lastX/Y to the current mouse position
    lastX = mouseX;
    lastY = mouseY;

    // change the target circles position by the 
    // distance the mouse has moved since the last
    // mousemove event
    draggingCircle.x += dx;
    draggingCircle.y += dy;

    //Change the control points
    allControlPoints[draggingCircle.id * 2] = switchCoordinateSyst(draggingCircle.x,0)[0]
    allControlPoints[draggingCircle.id * 2 +1] = switchCoordinateSyst(0,draggingCircle.y)[1]

    //Change the webGl Canvas
    changeValues()
    // redraw all the circles
    drawAll();
}


/**
 * Clear the canvas (point of controls)
 * @memberof TP3
 */
function clearCanvas(){
  // Reset points of control
  allControlPoints = []
  ctx.clearRect(0, 0, cw, ch);
  circles = [];
  lastId = 0;

  console.log("Clearing...")
  let canvasToClear = document.getElementById("glCanvas");
  let gl = canvasToClear.getContext("webgl");
  if (gl != null){
    gl.clearColor(0.125, 0.133, 0.145, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    console.log("Cleared!")
  }
  else console.log("Clearing failed...")
}


// Listen for clear button
document.getElementById('clear').addEventListener('click', clearCanvas);

// Listen for mouse events
canvas.addEventListener("mousedown", function (e) { handleMouseDown(e) });
canvas.addEventListener("mousemove", function (e) { handleMouseMove(e) });
canvas.addEventListener("mouseup", function (e) { handleMouseUp(e) });
canvas.addEventListener("mouseout", function (e) { handleMouseUp(e) });
document.onmousemove = function(e) {
  let x = Math.round(((e.clientX - offsetX - cw/2) / (cw/2))*100)/100;
  let y = Math.round(((e.clientY - offsetY - ch/2) / (ch/2) * -1)*100) / 100;
  e.target.title = `x: ${x}, y: ${y}`;
};