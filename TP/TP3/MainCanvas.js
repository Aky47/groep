/**
 * @file TP3 files
 * @version MainCanvas.js 1.0 ()
 * @author Groep Team
 * @module TP3 - De Boor
 * @copyright 2021
 */


/* eslint no-console:0 consistent-return:0 */
"use strict";


/**
 * Call after each update on client side. Check all inputs, if they are ok, redraw canvas.
 * @returns {null} - If there is no contrl points
 */
function changeValues() {
  let m = parseInt(document.getElementById("m").value);
  let knotVec = document.getElementById("nodeVector").value.split(new RegExp(',[ ]*')).map(Number);
  let ctrlPts = allControlPoints;
  if (ctrlPts.length == 0) return;
  const n = ctrlPts.length / 2 - 1

  if (m > n) {
    document.getElementById("m").value = n;
    m = n;
  } else if (m < 0) {
    document.getElementById("m").value = 0;
    m = 0;
  }
  const k = m + 1; // B-Spline curve order
  const kV = new Array(); // Knot vector

  if (knotVec.length != n+k+1) {
    for (let i = 0; i <= n+k; i++) kV.push(Math.round(i/(n+k)*100)/100); // Knot vector size = n + k
    document.getElementById("nodeVector").value = kV.map(knot => ` ${knot.toString()}`);
    knotVec = kV;
  }

  let pointNumber = parseInt(document.getElementById("pointNumber").value);
  document.getElementById("pointToSee").max = pointNumber - 1;
  const pointToSee = parseInt(document.getElementById("pointToSee").value);

  setAndDrawMainCanvas(knotVec, ctrlPts, m, pointNumber, pointToSee);
}


/**
 * De Boor function
 * @param {Array<Number>} knotVec - Array of knot positions
 * @param {Array<Number>} ctrlPts - Array of control points
 * @param {Number} m - Degree of B-spline
 * @param {Number} nPts - Number of point to draw
 * @param {Array<Number>} wPt - The point the client want to see splines (construction curves)
 * @returns {Array<Number>}
 */
function deBoor(knotVec, ctrlPts, m, nPts, wPt) {
  if (knotVec.length != (ctrlPts.length)/2 + (m + 1))
    throw console.error(`knotVec.length: ${knotVec.length}, it should be: ${ctrlPts.length/2+m+1}\nknotVec`, knotVec);

  const k = m + 1;
  const n = ctrlPts.length/2 - 1;
  let r = null;
  const bSpline = new Array();
  const t = new Array();
  const step = ((knotVec[n+1] - knotVec[k-1]) / (nPts));
  const pointConstructionCurve = new Array();
  const splines = Array.from(new Array(n-k+2), array => new Array());
  
  for (let i = knotVec[k-1]; i <= knotVec[n+1]; i += step)
    t.push(i);

  for (let w = 0; w < t.length; w++) {
    for (let h = k; h <= n+2; h++) {
      if (t[w] <= knotVec[h]) {
        r = h-1; // Find r such that w is between [tr, tr+1]
        break;
      }
    }
    let p = { x: new Array(), y: new Array() }
    let P = { x: new Array(), y: new Array() }
    // Split Xs and Ys points in two arrays
    for (let i = 0; i < n*2 + 1; i += 2) {
      p.x.push(ctrlPts[i]);
      p.y.push(ctrlPts[i+1]);
    }
    // Keep only used control points (from r-m to r)
    for (let i = 0; i < m+1; i++) {
      P.x.push(p.x[i+r-m])
      P.y.push(p.y[i+r-m])
    }
    console.log('r',r)
    console.log('P',P)
    for (let j = 1; j < m+1; j++) { // Depth (from left to right)
      for (let i = m; i > j-1; i--) { // Height (from top to bottom)
        const alpha = (t[w] - knotVec[i + r - m]) / (knotVec[i + 1 + r - j] - knotVec[i + r - m])
        P.x[i] = (1 - alpha) * P.x[i - 1] + alpha * P.x[i]
        P.y[i] = (1 - alpha) * P.y[i - 1] + alpha * P.y[i]
        splines[r-m].push(t[w]) // splines contains n-k+2 array (aka curve piece)
        splines[r-m].push(alpha)
      }
      if (w == wPt) {
        pointConstructionCurve.push(new Array())
        let index = pointConstructionCurve.length-1
        for (let i = j; i < P.x.length; i++) {
          pointConstructionCurve[index].push(P.x[i])
          pointConstructionCurve[index].push(P.y[i])
          console.log('w',w)
          console.log('P.x[i]',P.x[i])
          console.log('P.y[i]',P.y[i])
        }
      }
    }
    bSpline.push(P.x[m]);
    bSpline.push(P.y[m]);
  }
  return [ bSpline, pointConstructionCurve, splines ];
}


/**
 * Set the main canvas, then draw the b-spline depending on control points, knot vector and m+1 order.
 * Then, call setANdDrawSubCanvas in order to draw the splines (construction curves) of one specific point.
 * @param {Array<Number>} knotVec - Vector of knot, set by the client, or automaticly
 * @param {Array<Number>} ctrlPts - Array of contrlo points, set by the client on the canva
 * @param {Number} m - k order - 1
 * @param {Number} nPts - Number of point to draw
 * @param {Array<Number>} pointToSee - The point the client want to see splines (construction curves)
 * @returns {Null} - If WebGL didn't init well 
 */
function setAndDrawMainCanvas(knotVec, ctrlPts, m, nPts, pointToSee) {
  const canvas = document.querySelector("#glCanvas");
  const gl = canvas.getContext("webgl");

  if (!gl) {
    alert("ERROR WEBGL INIT FROM MAINJS");
    return;
  }

  // createProgramFromScripts will call bindAttribLocation
  // based on the index of the attibute names we pass to it
  var program = twgl.createProgramFromScripts(
      gl, 
      ["vshader", "fshader"], 
      ["apos2dition", "a_lengthSoFar"]);
  gl.useProgram(program);

  webglUtils.resizeCanvasToDisplaySize(gl.canvas);
  
  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
  gl.clearColor(0.125, 0.133, 0.145, 1.0);
  gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);
  
  /**
   * Return the distance between the last and the next point from the 3 dimensions
   * @param {Array<Number>} array - points array (3d)
   * @param {Number} ndx1 - last point
   * @param {Number} ndx2 - next point
   * @returns {Number} - New distance between the last and the next point from the 3 dimensions
   */
  function distance(array3d, ndx1, ndx2) {
    ndx1 *= 3;
    ndx2 *= 3;
    let dx = array3d[ndx1 + 0] - array3d[ndx2 + 0];
    let dy = array3d[ndx1 + 1] - array3d[ndx2 + 1];
    let dz = array3d[ndx1 + 2] - array3d[ndx2 + 2];
    return Math.sqrt(dx * dx + dy * dy + dz * dz);
  }

  
  /**
   * Draw an array of positions in 2d. C
   * @param {Array<Number>} pos2d - Array of 2d points
   * @param {Array<Number>} color - Color of the (dashed or not) line
   * @param {Boolean} isDashed - True if the line is dahed
   */
  function draw(pos2d, color, isDashed) {
    let pos3d = new Array();
    for (let i = 0; i < pos2d.length; i++) {
      pos3d.push(pos2d[i]);
      if ((i+1) % 2 == 0) pos3d.push(0);
    }

    let lengthSoFar = [0];  // the length so far starts at 0
    for (let ii = 1; ii < pos3d.length / 3; ++ii)
      lengthSoFar.push(lengthSoFar[ii - 1] + distance(pos3d, ii - 1, ii));
    
    let vertBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(pos3d), gl.STATIC_DRAW);
    gl.enableVertexAttribArray(0);
    gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 0, 0);
    
    vertBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, vertBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(lengthSoFar), gl.STATIC_DRAW);
    gl.enableVertexAttribArray(1);
    gl.vertexAttribPointer(1, 1, gl.FLOAT, false, 0, 0);
    
    // Set line/curve texture
    let pixels = new Array();
    for (let i = 0; i < 24; i++) {
      if (isDashed) { // If we want a dashedline
        if ((i%6) < 2) { // Dash
          for (let c = 0; c < 3; c++) pixels.push(color[c]) // RGB
          pixels.push(255) // Opacity
        } else { // Void
          for (let c = 0; c < 4; c++) pixels.push(0)
        }
      } else { // If we want a continuous line  
        for (let c = 0; c < 3; c++) pixels.push(color[c])
        pixels.push(255)
      }
    }
    
    let tex = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, tex);
    gl.texImage2D(
      gl.TEXTURE_2D, 0, gl.RGBA, pixels.length / 4, 1, 0,
      gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array(pixels));
    gl.texParameteri(
      gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(
      gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
      gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
    
    console.log('pos3d',pos3d)
    gl.drawArrays(gl.LINE_STRIP, 0, pos3d.length / 3);
  }

  const [ positions, pointConstructionCurve, splines ] = deBoor(knotVec, ctrlPts, m, nPts, pointToSee);

  // CONTROL POINTS DISPLAY
  draw(ctrlPts, [200.0, 200.0, 200.0], true)

  // B-SPLINES DISPLAY
  draw(positions, getColorValueFromHTML(), false)

  // CONSTRUCTION CURVES DISPLAY
  console.log('pointConstructionCurve',pointConstructionCurve)
  for (let i = 0; i < pointConstructionCurve.length; i++)
    draw(pointConstructionCurve[i], getNextRGB(i+1, pointConstructionCurve.length))

  // SPLINES DISPLAY
  setAndDrawSubCanvas(splines)
}


/**
 * Determine current RGB color for construction points display, according to saturation of colors.
 * @param {Number} degree - Current degree of the points
 * @param {Number} degreeMax - Maximum degree evaluated
 * @returns {Array<Number>} - Array of 3 values ranging from 0 to 255 such as (r,g,b)
 */
 function getNextRGB(degree, degreeMax) {
  let satLevel = Math.trunc(20/degreeMax) * degree;
  let r = (6 * (20 - satLevel));
  let g = 255.0 - (10 * (20-satLevel));
  let b = 255.0 - (10 * (20-satLevel));
  return [r,g,b];
}


/**
 * Returns the value of the HTML color picker to determine
 * the color of the curve to draw
 * @returns {Array<Number>} - Array of 3 values ranging from 0 to 255 such as (r,g,b)
 */
function getColorValueFromHTML(){
  let color = document.getElementById("curveColor").value
  let hex_code = color.split("");
  let r = parseInt(hex_code[1]+hex_code[2],16);
  let g = parseInt(hex_code[3]+hex_code[4],16);
  let b = parseInt(hex_code[5]+hex_code[6],16);
  return ([r, g, b])
}




// Don't delete pls - old version
    /*
    //console.log("____p")
    //console.log([[p.x[0],p.x[1],p.x[2],p.x[3],p.x[4]],[p.y[0],p.y[1],p.y[2],p.y[3],p.y[4]]])
    for (let j = 1; j < k; j++) { // Depth (from left to right)

      console.log(`\t____ j = ${j}`)
      
      for (let i = r-k+j+1; i < r+1; i++) { // Height (from top to bottom)
        
        console.log(`\t\t____ i = ${i}`)
        console.log(`\t\t\t____ P(${i},${j}) = P(${i-1},${j-1}) & P(${i},${j-1})`)
        console.log(`\t\t\t____ t[w] = ${t[w]}`)
        const alpha = (knotVec[i+k-j] - knotVec[i] > 0) ? (t[w] - knotVec[i])/(knotVec[i+k-j] - knotVec[i]) : 0;
        console.log(`\t\t\t____ alpha = (${Math.round(t[w] * 100) / 100} - ${Math.round(knotVec[i] * 100) / 100}) / (${Math.round(knotVec[i+k-j] * 100) / 100} - ${Math.round(knotVec[i] * 100) / 100}) = ${Math.round(alpha * 100) / 100}`)
        console.log(`\t\t\t____ Px = ((1 - ${Math.round(alpha * 100) / 100}) * ${Math.round(p.x[i-1] * 100) / 100} + ${Math.round(alpha * 100) / 100} * ${Math.round(p.x[i] * 100) / 100}) = ${Math.round(((1 - alpha) * p.x[i-1] + alpha * p.x[i])*100)/100}`)
        console.log(`\t\t\t____ Py = ((1 - ${Math.round(alpha * 100) / 100}) * ${Math.round(p.y[i-1] * 100) / 100} + ${Math.round(alpha * 100) / 100} * ${Math.round(p.y[i] * 100) / 100}) = ${Math.round(((1 - alpha) * p.y[i-1] + alpha * p.y[i])*100)/100}`)
        p.x[i] = (1 - alpha) * p.x[i-1] + alpha * p.x[i]
        p.y[i] = (1 - alpha) * p.y[i-1] + alpha * p.y[i]
      }
    }
    console.log(`\t\t\tP_(${k-2},${m}) = (${Math.round(p.x[m] * 100) / 100}, ${Math.round(p.y[m] * 100) / 100})`)
    bSpline.push(p.x[m]);
    bSpline.push(p.y[m]);*/