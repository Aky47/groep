/**
 * @file SubCanvas.js
 * @version SubCanvas.js 1.0
 * @module TP3 - De Boor
 */


/* eslint no-console:0 consistent-return:0 */
"use strict";


/**
 * WebGL function
 * @param {Object} gl - WebGL Object
 * @param {Object} type - WebGL Object
 * @param {Object} source - WebGL Object
 * @returns {Object}
 */
function createShader2(gl, type, source) {
  var shader2 = gl.createShader(type);
  gl.shaderSource(shader2, source);
  gl.compileShader(shader2);
  var success2 = gl.getShaderParameter(shader2, gl.COMPILE_STATUS);
  if (success2) {
    return shader2;
  }
  console.log(gl.getShaderInfoLog(shader2));
  gl.deleteShader(shader2);
}


/**
 * WebGL function
 * @param {Object} gl - WebGL Object
 * @param {Object} vertexShader2 - WebGL Object
 * @param {Object} fragmentShader2 - WebGL Object
 * @returns {Object}
 */
function createProgram(gl, vertexShader2, fragmentShader2) {
  var program2 = gl.createProgram();
  gl.attachShader(program2, vertexShader2);
  gl.attachShader(program2, fragmentShader2);
  gl.linkProgram(program2);
  var success2 = gl.getProgramParameter(program2, gl.LINK_STATUS);
  if (success2) {
    return program2;
  }
  console.log(gl.getProgramInfoLog(program2));
  gl.deleteProgram(program2);
}


/**
 * Set, the sub canvas, then draw each 'r-m' spline such as (x: t[w], y: alpha)
 * @param {Array<Number>} positions - Array of 2d points (construction curve point)
 * @returns {null} - If WebGL didn't init well 
 */
function setAndDrawSubCanvas(splines) {
  const canvas = document.querySelector("#glCanvas2");
  const gl = canvas.getContext("webgl");

  if (!gl) {
    alert("ERROR WEBGL INIT FROM MAINJS");
    return;
  }

  var vertexShaderSource2 = document.querySelector("#vertex-shader-2d-2").text;
  var fragmentShaderSource2 = document.querySelector("#fragment-shader-2d-2").text;
  var vertexShader2 = createShader2(gl, gl.VERTEX_SHADER, vertexShaderSource2); // Afficher points / figures
  var fragmentShader2 = createShader2(gl, gl.FRAGMENT_SHADER, fragmentShaderSource2); // Couleurs, ombre, etc.
  var program2 = createProgram(gl, vertexShader2, fragmentShader2);
  var positionAttributeLocation2 = gl.getAttribLocation(program2, "a_position");
  var positionBuffer2 = gl.createBuffer();

  gl.useProgram(program2);
  gl.enableVertexAttribArray(positionAttributeLocation2);
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer2);

  webglUtils.resizeCanvasToDisplaySize(gl.canvas);

  gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
  gl.clearColor(0.125, 0.133, 0.145, 1.0);
  gl.clear(gl.COLOR_BUFFER_BIT|gl.DEPTH_BUFFER_BIT);
  gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer2);

  // Tell the attribute how to get data out of positionBuffer2 (ARRAY_BUFFER)
  const size = 2;
  const type = gl.FLOAT;
  const normalize = false;
  const stride = 0;
  const offset = 0;
  const primitiveType = gl.LINE_STRIP;
  
  for (let i = 0; i < splines.length; i++) {
    let spline = splines[i].map(x => x*2 -1);
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(spline), gl.STATIC_DRAW);
    gl.vertexAttribPointer(positionAttributeLocation2, size, type, normalize, stride, offset);
    gl.drawArrays(primitiveType, offset, spline.length/2)
  }
}


/**
 * Clear the 2nd cavnas
 */
function clearCanvas2(){
  console.log("Clearing 2nd canvas...")
  let canvas = document.getElementById("glCanvas2");
  let gl = canvas.getContext("webgl");
  if (gl != null){
    gl.clearColor(0.125, 0.133, 0.145, 1.0);
    gl.clear(gl.COLOR_BUFFER_BIT);
    console.log("2nd canvas cleared!")
  }
  else console.log("2nd canvas clearing failed...")
}


// Listen for the clear button
document.getElementById('clear').addEventListener('click', clearCanvas2);