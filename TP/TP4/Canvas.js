/**
 * @file TP4 files
 * @version MainCanvas.js 1.0 ()
 * @author Groep Team
 * @copyright 2021
 */


/* eslint no-console:0 consistent-return:0 */
"use strict";


let canvas = document.getElementById("glCanvas");
let camera = new Camera();
let engine = new Engine(canvas,camera);
let color;
let pos;


//updateCam(_pos, _color);
changeValues();



/**
 * Call after each update on client side. Check all inputs, if they are ok, redraw canvas.
 * @param {*} offsetY - Time-dependant, allows to create pitch variation
 */
function changeValues(offsetY = 0) {
  // Initialization
  let [ y0, y1, y2 ] = [ Math.sin(offsetY), Math.sin(offsetY+.5), Math.sin(offsetY+1) ];
  let arrayOfControlPoints = [
    [ -1-10,   4+y0,  4.5, 11-10, 3.9+y1,  7.6, 22-10, 4.2+y2,  1.2 ],  // courbe haut gauche
    [  0-10, 0.6+y0,  3.8, 11-10,   0+y1,    7, 21-10, 1.1+y2,  0.4 ],  // courbe bas gauche
    [  0-10, 0.6+y0, -3.8, 11-10,   0+y1,   -7, 21-10, 1.1+y2, -0.4 ],  // courbe bas droite
    [ -1-10,   4+y0, -4.5, 11-10, 3.9+y1, -7.6, 22-10, 4.2+y2, -1.2 ]]; // courbe haut droite
  
  const m = 2;
  const k = m + 1; // B-Spline curve order
  const n = arrayOfControlPoints[0].length / 3 - 1 ;
  const numberofPoints = 25;
  const knotVec = [];
  for (let i = 0; i <= n+k; i++) knotVec.push((i < (n+k)/2) ? 0 : 1);

  const palette = [
    168, 132, 84,
    151, 118, 75,
    135, 106, 67  ]

  let arrayPoints = [
     0-10, 0.6+y0,  3.8, // Back face
    -1-10,   4+y0,  4.5,
     0-10, 0.6+y0, -3.8,
    -1-10,   4+y0,  4.5,
     0-10, 0.6+y0, -3.8,
    -1-10,   4+y0, -4.5,
    21-10, 1.1+y2,  0.4, // Front face
    22-10, 4.2+y2,  1.2,
    21-10, 1.1+y2, -0.4,
    22-10, 4.2+y2,  1.2,
    21-10, 1.1+y2, -0.4,
    22-10, 4.2+y2, -1.2];

  let arrayColors = [
    107, 84, 53, // Back face
    107, 84, 53,
    107, 84, 53,
    107, 84, 53,
    107, 84, 53,
    107, 84, 53,
    176, 144, 101, // Front face
    176, 144, 101,
    176, 144, 101,
    176, 144, 101,
    176, 144, 101,
    176, 144, 101,];

  let arrayOfDeboor = []; // Will contain De Boor curves for the boat's sides

  const [ seaPoints, seaColors ] = createSea(offsetY); // Collects sea components according to the offset

  arrayPoints = arrayPoints.concat(seaPoints);
  arrayColors = arrayColors.concat(seaColors);

  // Compute
  for (let i = 0; i < arrayOfControlPoints.length; i++) {
    arrayOfDeboor.push(deBoor(knotVec, arrayOfControlPoints[i], m, numberofPoints)); // 4 curves connecting the stern and the bow of the boat
  }

  function push3dPointFromDeBoor(i, j) {
    arrayPoints.push(
      arrayOfDeboor[i][j*3+0],
      arrayOfDeboor[i][j*3+1],
      arrayOfDeboor[i][j*3+2])
  }

  for (let i = 0; i < arrayOfDeboor.length - 1; i++) {
    for (let j = 0; j < numberofPoints; j++) { // (numberofPoints - 1) === (10 - 1) === 9
      push3dPointFromDeBoor(  i,   j) // Triangle 1
      push3dPointFromDeBoor(i+1,   j)
      push3dPointFromDeBoor(  i, j+1)
      push3dPointFromDeBoor(i+1,   j) // Triangle 2
      push3dPointFromDeBoor(  i, j+1)
      push3dPointFromDeBoor(i+1, j+1)
      for (let l = 0; l < 6; l++) {
        arrayColors.push(
          palette[(i) % (4 - 1)*3+0],
          palette[(i) % (4 - 1)*3+1],
          palette[(i) % (4 - 1)*3+2]);
      }
    }
  }

  pos = arrayPoints;
  color = arrayColors;
  updateCam();
}



/**
 * Set up a timer to create an offset in order to animate the scene
 */
document.getElementById("anim_button").onclick = function() {
  document.getElementById("anim_button").disabled = true;
  let start = Date.now();

  let timer = setInterval(function() {
    let timePassed = Date.now() - start;

    let offset = Math.round(timePassed/40) / 10

    console.log('timePassed',offset)
    changeValues(offset)

    document.getElementById("anim_button").innerHTML = `Animation started: ${Math.round((timePassed/1000- 10)*10)/10} s`;

    if (timePassed > 10000) {
      document.getElementById("anim_button").innerHTML = `Hit me to start the animation!`;
      document.getElementById("anim_button").disabled = false;
      clearInterval(timer);
    }

  }, 40);
}



/**
 * Generate 3d sea surface + color variation (sinus) depending on the time
 * @param {Number} offset - Time-dependant, allows to create pitch variation
 * @returns {Array<Array<Number>>}  
 */
function createSea(offset) {
  const [ minX, minZ, maxX, maxZ ] = [ -100, -100, 100, 100 ];
  const step = 10;
  const offsetStep = 0.4; // hardCoded --> @see {timer}

  let [ seaPoints, seaColors ] = [ [], [] ];

  for (let x = minX; x < maxX; x += step) {
    for (let z = minZ; z < maxZ; z += step) {

      let [ y0, y1, y2, y3 ] = [ // Constant dependent on the corner of the rectangle
        ((x     )+(z+step))/step,
        ((x+step)+(z+step))/step, 
        ((x     )+(z     ))/step,
        ((x+step)+(z     ))/step]

      let y = [ // Calculate the sinusoidal offset according to the corner constant
        Math.sin(offset + y0 * offsetStep),
        Math.sin(offset + y1 * offsetStep),
        Math.sin(offset + y2 * offsetStep),
        Math.sin(offset + y3 * offsetStep)]
      
      seaPoints.push( // Create surface
        x,      y[0], z+step,
        x+step, y[1], z+step,
        x,      y[2],      z,
        x+step, y[1], z+step,
        x,      y[2],      z,
        x+step, y[3],      z);

      if ((x+z) / step % 2 == 0) { // Create linked texture color
        seaColors.push(
          212, 241, 200,
          150, 200, 255,
          150, 200, 255,
          150, 200, 255,
          150, 200, 255,
          212, 241, 200
        );
      } else {
        seaColors.push(
          150, 200, 255,
          212, 241, 200,
          212, 241, 200,
          212, 241, 200,
          212, 241, 200,
          150, 200, 255,
        )
      }
    }
  }
  return [ seaPoints, seaColors ];
}



/**
 * 3D De Boor function 
 * @param {Array<Number>} knotVec - Array of knot positions
 * @param {Array<Number>} ctrlPts - Array of 3d control points
 * @param {Number} m - Degree of B-spline
 * @param {Number} nPts - Number of point to draw
 * @returns {Array<Number>}
 */
function deBoor(knotVec, ctrlPts, m, nPts) {
  if (knotVec.length != (ctrlPts.length)/3 + (m + 1))
    throw console.error(`knotVec.length: ${knotVec.length}, it should be: ${ctrlPts.length/3+m+1}\nknotVec`, knotVec);

  const k = m + 1;
  const n = ctrlPts.length/3 - 1;
  let r = null;
  const bSpline = new Array();
  const t = new Array();
  const step = ((knotVec[n+1] - knotVec[k-1]) / (nPts - Math.floor(nPts / 40 + 1)));
  
  for (let i = knotVec[k-1]; i <= knotVec[n+1]; i += step)
    t.push(i);

  for (let w = 0; w < t.length; w++) {
    for (let h = k; h <= n+2; h++) {
      if (t[w] <= knotVec[h]) {
        r = h-1; // Find r such that w is between [tr, tr+1]
        break;
      }
    }
    let p = { x: new Array(), y: new Array(), z: new Array() }
    let P = { x: new Array(), y: new Array(), z: new Array() }
    // Split Xs and Ys points in two arrays
    for (let i = 0; i < n*3 + 2; i += 3) {
      p.x.push(ctrlPts[i]);
      p.y.push(ctrlPts[i+1]);
      p.z.push(ctrlPts[i+2]);
    }
    // Keep only used control points (from r-m to r)
    for (let i = 0; i < m+1; i++) {
      P.x.push(p.x[i+r-m])
      P.y.push(p.y[i+r-m])
      P.z.push(p.z[i+r-m])
    }
    for (let j = 1; j < m+1; j++) { // Depth (from left to right)
      for (let i = m; i > j-1; i--) { // Height (from top to bottom)
        const alpha = (t[w] - knotVec[i + r - m]) / (knotVec[i + 1 + r - j] - knotVec[i + r - m])
        P.x[i] = (1 - alpha) * P.x[i - 1] + alpha * P.x[i]
        P.y[i] = (1 - alpha) * P.y[i - 1] + alpha * P.y[i]
        P.z[i] = (1 - alpha) * P.z[i - 1] + alpha * P.z[i]
      }
    }
    bSpline.push(P.x[m]);
    bSpline.push(P.y[m]);
    bSpline.push(P.z[m]);
  }
  return bSpline;
}



/**
 * Update the scene with new camera settings
 */
function updateCam() {

  let radToDeg = (r) => Math.round(r * 180 / Math.PI * 10) / 10;
  let degToRad = (d) => d * Math.PI / 180;

  let rotx = degToRad(parseInt(document.getElementById("rotx").value));
  let roty = degToRad(parseInt(document.getElementById("roty").value));
  let rotz = degToRad(parseInt(document.getElementById("rotz").value));
  let posx = parseInt(document.getElementById("posx").value);
  let posy = parseInt(document.getElementById("posy").value);
  let posz = parseInt(document.getElementById("posz").value);

  document.getElementById("labrotx").innerHTML = `x rotation: ${radToDeg(rotx)}°`;
  document.getElementById("labroty").innerHTML = `y rotation: ${radToDeg(roty)}°`;
  document.getElementById("labrotz").innerHTML = `z rotation: ${radToDeg(rotz)}°`;
  document.getElementById("labposx").innerHTML = `x position: ${posx}`;
  document.getElementById("labposy").innerHTML = `y position: ${posy}`;
  document.getElementById("labposz").innerHTML = `z position: ${posz}`;

  let surface = new Surface(pos,color);
  let transform = new Transform();
  engine.camera.setPosition([posx, posy, posz]);
  transform.apply("ROTATION", [rotx, roty, rotz]) //Rotate the y coodinate of PI/4
  engine.clear();
  engine.drawSurface(surface, transform)
}