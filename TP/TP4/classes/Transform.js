/**
 * @file TP4 files
 * @version Transform.js 1.0 ()
 * @author Groep Team
 * @memberof TP4 - 3D
 * @copyright 2021
 */
/**Class representing a transformation concerning a Surface*/
class Transform {

  //Tried to used Symbolic constant, didn't went well
  //static SCALE = 0;
  //static TRANSLATION = 1;
  //static ROTATION = 2;





  /**
   * Create a Transformation 
   * @param {Array} scale - [x,y,z] 
   * @param {Array} translation - [x,y,z]
   * @param {Array} rotation - [x,y,z]
   */
  constructor() {
    this.scale = [1,1,1];
    this.rotation = [0,0,0]
    this.translation = [0,0,0]
  }




  /**
   * Set the scale
   * @param {Array} scale - The scale value [x,y,z]
   */
  setScale(scale){
    if(scale.length != 3){
        throw console.error(`Error : invalid argument given : [${target}], \n Length is supposed to be 3 : ${target.length} was given`);
    }
    this.scale = scale
  }

    /**
     * Get the scale value
     * @return {Array} - scale value
     */
  getScale(){
    return this.scale;
  }






  /**
   * Set the rotation
   * Rotation are in radians NOT DEGREES
   * @param {Array} rotation - The rotation value [x,y,z]
   */
  setRotation(rotation){
    if(rotation.length != 3){
        throw console.error(`Error : invalid argument given : [${target}], \n Length is supposed to be 3 : ${target.length} was given`);
    }
    this.rotation = rotation
  }

  /**
   * Get the rotation value
   * @return {Array} - rotation value
   */
  getRotation(){
      return this.rotation;
  }



    /**
   * Set the translation
   * @param {Array} translation - The translation value [x,y,z]
   */
  setTranslation(translation){
    if(translation.length != 3){
        throw console.error(`Error : invalid argument given : [${target}], \n Length is supposed to be 3 : ${target.length} was given`);
    }
    this.translation = translation
  }

  /**
   * Get the translation value
   * @return {Array} - translation value
   */
  getTranslation(){
    return this.translation;
  }


  /**
   * apply a given operation according to the current values 
   * 
   * @param {String} mode - Which operation to do, either "SCALE", "TRANSLATION" or "ROTATION"
   * @param {String} arr - Values to use for the given mode
   * 
   */
  apply(mode,arr){
    if(mode == "SCALE"){
      this.scale = [this.scale[0] + arr[0], this.scale[1] + arr[1], this.scale[2] + arr[2]]
    }
    if(mode == "TRANSLATION"){
      this.translation = [this.translation[0] + arr[0], this.translation[1] + arr[1], this.translation[2] + arr[2]]
    }
    if(mode == "ROTATION"){
      this.rotation = [this.rotation[0] + arr[0], this.rotation[1] + arr[1], this.rotation[2] + arr[2]]
    }
  }


}