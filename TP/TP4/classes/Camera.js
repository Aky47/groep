/**
 * @file TP4 files
 * @version Camera.js 1.0 ()
 * @author Groep Team
 * @memberof TP4 - 3D
 * @copyright 2021
 */

/**Class representing the camera*/
class Camera{
    
    constructor(){
        this.cameraPosition = [0, 0, 200];
        this.target = [0, 0, 0];
        this.up = [0, 1, 0]; //Allows m4 to know which way component is y (used for vectorial product)
        this.cameraMatrix = m4.lookAt(this.cameraPosition, this.target, this.up);
    }

    /**
     * Used internally to compute the cameraMatrix on every change
     */
    onAttributeChange(){
        this.cameraMatrix = m4.lookAt(this.cameraPosition, this.target, this.up);
    }

    /**
     * Set the camera's postion
     * @param {Array} position - The position value [x,y,z]
     */
    setPosition(position){
        if(position.length != 3){
            throw console.error(`Error : invalid argument given : [${position}], \n Length is supposed to be 3 : ${position.length} was given`);
        }
        this.cameraPosition = position
        this.onAttributeChange()
    }

    /**
     * Get the camera's position
     * @return {Array} - position value
     */
    getPosition(){
        return this.cameraPosition;
    }


    /**
     * Set the camera's focus onto a given point
     * @param {Array} target - The target value [x,y,z]
     */
    setTarget(target){
        if(target.length != 3){
            throw console.error(`Error : invalid argument given : [${target}], \n Length is supposed to be 3 : ${target.length} was given`);
        }
        this.target = target
        this.onAttributeChange()
    }

    /**
     * Get the camera's target position
     * @return {Array} - position value
     */
    getTarget(){
        return this.target;
    }

   
  /**
   * apply a given array of valyes according to the mode 
   * 
   * @param {String} mode - Which operation to do, either "POSITION" or "TARGET" 
   * @param {String} arr - Values to use for the given mode
   * 
   */
  apply(mode,arr){
    if(mode == "POSITION"){
      this.cameraPosition = [this.cameraPosition[0] + arr[0], this.cameraPosition[1] + arr[1], this.cameraPosition[2] + arr[2]]
    }
    if(mode == "TARGET"){
      this.target = [this.target[0] + arr[0], this.target[1] + arr[1], this.target[2] + arr[2]]
    }

    this.onAttributeChange();
  }
}
