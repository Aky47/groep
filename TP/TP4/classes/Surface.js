/**
 * @file TP4 files
 * @version Surface.js 1.0 ()
 * @author Groep Team
 * @memberof TP4 - 3D
 * @copyright 2021
 */

/**Class representing a Surface*/
class Surface {
    /**
     * 
     * @param {Array} pos - 1 Dimensional Array containing successively x,y,z
     * @param {Array} color - 1 Dimensional Array containing successively r,g,b (Must range between 0-255)
     */
    constructor(pos,color) {

      if(pos.length != color.length){
        throw("Error : Invalid Argument \n Length of positions and color does not match !")
      }
      if(pos.length % 3 != 0 ){
        throw("Error : Invalid Argument \n Length of position couldn't be divided by 3")
      }  
      this.pos = pos;
      this.color = color;
    }
  

  /**
   * Set the surface points
   * @param {Array} pos - The positions value [x1,y1,z1,x2,y2...]
   */
  setPosition(pos){
    if(pos.length % 3 != 0 ){
      throw("Error : Invalid Argument \n Length of position couldn't be divided by 3")
    }
    this.pos = pos
  }
  
  /**
   * Get the surface points
   * @return {Array} - position value
   */
  getPosition(){
      return this.pos;
  }



  /**
 * Set the surface color
 * @param {Array} color - The color value [r1,g1,b1,r2,g2...]
 */
    setColor(color){
    if(color.length % 3 != 0 ){
      throw("Error : Invalid Argument \n Length of color couldn't be divided by 3")
    }
    this.color = color
  }
  
  /**
   * Get the surface color
   * @return {Array} - color value
   */
  getcolor(){
      return this.color;
  }

}